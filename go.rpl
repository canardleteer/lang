-- -*- Mode: rpl; -*-                                                                               
--
-- go.rpl
--
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHORS: Jordan Connor, Trevor Gasdaska, Spatika Ganesh, Maya Shankar, Jamie A. Jennings

package go

-- N.B. The --wholefile option is needed for many of these patterns because they
-- can span lines.

alias line_end = "\n"
alias statement_end = ";"

-------------------------------
-- Inline Comments
--
-- Matches in line comments starting with a '//'. Also
-- captures the code preceding the comment on the same line.
--
-- Run: rosie -wholefile line_comments <go-source-file>
-- Structure: line_comments                      // Parent pattern to extract all comments from file
--                \- line_comment                // Matches a single line comment
--                    \- line_comment_context    // The code on the line preceding the comment
--                    \- line_comment_body       // Matches the whole comment text
--                        \- line_comment_text   // The text of the comment
-------------------------------

alias line_comment_start= "//"
line_comment_text= {!line_end .}*
alias line_comment_pre= {!line_comment_start !line_end .}*
line_comment_context= {!line_comment_start !line_end .}*
line_comment_body= {line_comment_start line_comment_text}
line_comment= {line_comment_context line_comment_body}
line_comments= {line_comment / .}*

-- Aliased version of the pattern for use in other patterns
alias alias_line_comment_text= {!line_end .}*
alias alias_line_comment_context= {!line_comment_start !line_end .}*
alias alias_line_comment_body= {line_comment_start alias_line_comment_text}
alias alias_line_comment= {alias_line_comment_context alias_line_comment_body}

-------------------------------
-- Block Comments
--
-- Matches block comments. Block comments in go
-- start with /* and end with */
--
-- Run: rosie -wholefile block_comments <go-source-file>
-- Structure: block_comments                  // Parent pattern to match all block comments
--                \- block_comment            // Matches a single block comment
--                    \- block_comment_body   // Matches the body of a block comment
-------------------------------

alias block_comment_start = "/*"
alias block_comment_end = "*/"
alias block_comment_pre= {!block_comment_start .}*
block_comment_body= {!block_comment_end .}*
block_comment= {block_comment_start block_comment_body block_comment_end}
block_comments= {block_comment_pre block_comment}*

-- Aliased version of the pattern for use in other patterns
alias alias_block_comment_body= {!block_comment_end .}*
alias alias_block_comment= {block_comment_start alias_block_comment_body block_comment_end}

-------------------------------
-- String Literals
--
-- Currently matches for typical Go string and character literals.
-- Does allow for escaped single and double quotes
--
-- Run: rosie -wholefile strings <Go-source-file>
-- Structure: strings        // Parent pattern to match all string literals
--                \- string  // Pattern to match a single string
-------------------------------

string = (["] {([\\] / !["]) .}* ["]) / (['] {([\\] / ![']) .}* ['])
strings = {alias_line_comment / alias_block_comment / string / .}*

--A "aliased" version is provided to allow for suppressed output in other patterns
alias alias_string = (["] {([\\] / !["]) .}* ["]) / (['] {([\\] / ![']) .}* ['])

-------------------------------
-- Dependencies
--
-- Matches dependencies declared with "import" and
-- packages listed with "package".
--
-- Run: rosie -wholefile dependencies <go-source-file>
-- Structure: dependencies                  // Parent pattern to match all dependencies in a file
--                \- package                // Matches a single package
--                    \- package_text       // Name of the matched package
--                \- dependency             // Matches a single import
--                    \- dependencies_text  // Name of the matched import
--                \- dependency_factor      // Matches a collective import
--                    \- import_list        // Matches all imports in matchedcollection
-------------------------------

alias import = "import "
alias indent = {"\t" / " "}*
dependencies_text = {!{["] line_end} .}*
dependency = {{import ["]} dependencies_text {["]}}
import_list = {indent ["] dependencies_text {["] line_end} .}*
dependencies_factor = {import "(" line_end import_list}
alias dependencies_pre = {!import !line_end .}*

package_text = {!statement_end !line_end .}*
package = "package " package_text

dependencies = {{package / dependency / dependencies_factor / dependencies_pre} line_end}*

-------------------------------
-- Functions
--
-- Pattern to match function definitions. Does not
-- capture function bodies or constructors.
--
-- Run: rosie -wholefile functions <go-source-file>
-- Structure: functions                     // Parent pattern that matches all function definitions
--                \- function           // Matches an entire function definition
--                    \- function_name      // Name of the function
--                    \- parameters         // Matches a list of parameters
--                        \- single_param   // Matches a parameter in list
--                    \- return             // Matches a list of return values
--                        \- single_return  // Matches a return value in list
-------------------------------

func = "func "
alias start_paren = "("
alias end_paren = ")"
alias whitespace = [:space:]

function_name = {!start_paren .}*
single_param = {!end_paren ![,] .}*
alias multi_param =  {[,] single_param }*
parameters = start_paren single_param multi_param end_paren

single_return = {!end_paren ![,] .}*
alias multi_return =  {[,] single_return}*
return = start_paren single_return multi_return end_paren
function = func function_name parameters? return?

functions = {function / .}*

-------------------------------
-- Structs
--
-- Pattern to match struct definitions. Does not
-- capture struct bodies.
--
-- Run: rosie -wholefile structs <go-source-file>
-- Structure: structs                     // Parent pattern that matches all struct definitions
--                \- struct           // Matches an entire struct definition
--                    \- struct_name      // Name of the struct
-------------------------------

alias type = "type "
alias struct_decl = "struct "
struct_name = {!struct_decl .}*

struct = type struct_name struct_decl
structs = {struct / .}*

file = {line_comment / block_comment / package / dependency / function / struct / .}*
