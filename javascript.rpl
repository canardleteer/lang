-- -*- Mode: rpl; -*-                                                                                   
--
-- javascript.rpl
--
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHORS: Jordan Connor, Trevor Gasdaska, Spatika Ganesh, Maya Shankar, Jamie A. Jennings

package javascript

-- N.B. The --wholefile option is needed for many of these patterns because they
-- can span lines.

alias statement_end = ";"
alias line_end = "\n"

alias start_paren = "("
alias end_paren = ")"
alias start_block = "{"
alias end_block = "}"
alias whitespace = [:space:]

alias esc = "\\"

---------------------------------------------------------------------
-- String Literals
--Catches strings in the file any text inside the charactesrs " x "
-- Also catches  \" to continue matching the string if  " is escaped
--
-- Run : rosie -wholefile "strings" <javascript-source-file>
-- Structure : strings                       // Parent pattern to extract all strings from file
--                \- strings_text            // Matches the text enclosed within "" and ''
---------------------------------------------------------------------
alias bt = [`]						    -- back tick
alias dq = ["]						    -- double quote
alias sq = [']						    -- single quote
bt_string_text = { !bt {esc bt} / . }*
dq_string_text = { !dq {esc dq} / . }*
sq_string_text = { !sq {esc sq} / . }*
bt_string = { bt bt_string_text bt }
dq_string = { dq dq_string_text dq }
sq_string = { sq sq_string_text sq }
string = dq_string / sq_string / bt_string
strings = findall:string

---------------------------------------------------------------------
-- Inline Comments
-- Matches in line comments starting with a '//'. Also
-- captures the code preceding the comment on the same line.
--
-- Run: rosie -wholefile "line_comments" <javascript-source-file>
-- Structure: line_comments                      // Parent pattern to extract all line comments from file
--                \- line_comment                // Matches a single line comment
--                    \- line_comment_context    // The code on the line preceding the comment
--                    \- line_comment_body       // Matches the whole comment text
--                        \- line_comment_text   // The text of the comment
---------------------------------------------------------------------
alias line_comment_start= "//"
line_comment_text= {!line_end .}*
alias line_comment_pre= {!line_comment_start !line_end .}*
line_comment_context= {!line_comment_start !line_end .}*
line_comment_body= {line_comment_start line_comment_text}
line_comment= {line_comment_context line_comment_body}
line_comments= {{line_comment / line_comment_body / line_comment_pre} .}*

alias alias_line_comment_start= "//"
alias alias_line_comment_text= {!line_end .}*
alias alias_line_comment_pre= {!alias_line_comment_start !line_end .}*
alias alias_line_comment_context= {!alias_line_comment_start !line_end .}*
alias alias_line_comment_body= {alias_line_comment_start alias_line_comment_text}
alias alias_line_comment= {alias_line_comment_context alias_line_comment_body}
alias alias_line_comments= {line_comment / .}*

---------------------------------------------------------------------
--Block Comments
-- Matches in line comments starting with a '/*' '*/'. Also
-- captures the code preceding the comment on the same line.
--
-- Run: rosie -wholefile "block_comments" <javascript-source-file>
-- Structure: block_comments                      // Parent pattern to extract all block comments from file
--                \- block_comment                // Matches a single block comment
--                    \- block_comment_pre        // The code on the text preceding the comment
--                    \- block_comment_body       // Matches the whole block comment text
---------------------------------------------------------------------
alias block_comment_start = "/*"
alias block_comment_end = "*/"
alias block_comment_pre= {!block_comment_start .}*
block_comment_body= {!block_comment_end .}*
block_comment= { block_comment_start block_comment_body block_comment_end}
block_comments= {block_comment_pre block_comment }*

alias alias_block_comment_start = "/*"
alias alias_block_comment_end = "*/"
alias alias_block_comment_pre= {!alias_block_comment_start .}*
alias alias_block_comment_body= {!alias_block_comment_end .}*
alias alias_block_comment= { alias_block_comment_start alias_block_comment_body alias_block_comment_end}
alias alias_block_comments= {alias_block_comment_pre alias_block_comment }*

---------------------------------------------------------------------
-- Dependencies
-- Matches all the import statements present in the source file
-- Run: rosie -wholefile "line_comments" <javascript-source-file>
-- Structure: dependencies                           // Parent pattern to extract all import statements from file
--                \- dependencies_pre                // Code other than dependencies
--                \- dependency                      // Matches a single line comment
--                    \- dependencies_lonemember     // Matches import statement with one member
--                        \- member                  // The member that is imported
--                            \- member_text         // The member name
--                    \- dependencies_multimember    // Matches import statement with multiple members
--                        \- multi_members           // The multiple members imported
--                            \- member              // A member of the multi_members
--                                \- member_text     // The member name
--                    \- dependencies_module         // Matches import statement with just module name
--                        \- module_text             // The module name
---------------------------------------------------------------------
alias import = "import "
alias from = " from "
alias as = " as "
alias comma = [,]
alias brace_start = {"{"}
alias brace_end = {"}"}
member_alias = { !brace_end !comma !from !statement_end !line_end  .}*
member_name = { !brace_end !comma !as !from !statement_end !line_end  .}*
alias member_alias_text = as member_alias
member = { member_name member_alias_text? }
alias member_text = { brace_start? member brace_end? }

module = { !statement_end !line_end .}*
alias module_text = {module}

alias dependencies_lonemember = {import member_text from module_text }

dependencies_module = {import module_text}
multi_members = {member {comma member_text}* }
alias dependencies_multimember = {import multi_members from module_text}
alias dependencies_pre = {!import !line_end .}*

dependency = {dependencies_lonemember / dependencies_multimember / dependencies_module}

dependencies = { { dependency / dependencies_pre} {line_end / statement_end}}*

---------------------------------------------------------------------
-- Function Definitions
-- Catches function calls present in the file
-- Run: rosie -wholefile "line_comments" <javascript-source-file>
-- Structure: functions                              // Parent pattern to extract all function calls
--                \- function_call                   // Catches a function call
--                     \- function                   // Catches just the "function" keyword
--                     \- function_name              // Matches the name of the function
--                     \- parameters                 // Matches the parameters for the function call
--                         \- multip_param           // Matches all parameters
--                             \- single_param       // matches a single parameter
---------------------------------------------------------------------

alias pre_function = {statement_end / line_end / [.] / [=] / "return"  . }
alias function_call = {pre_function? "function" }
function_name = {!line_end !start_paren .}*
single_param = { !end_paren ![,] .}+
alias multip_param = (single_param ",")* single_param
parameters =  "(" multip_param? ")"

alias function_body_clean = {!end_block .}*
function_body = {whitespace start_block function_body_clean end_block}
function =  function_call  function_name parameters

functions = { alias_line_comment / alias_block_comment / function / .}*

---------------------------------------------------------------------
-- Exceptions
-- Throw statements are caught with this pattern
-- Run: rosie -wholefile "exceptions" <javascript-source-file>
-- Structure: exceptions                              // All Exceptions statements
--                \- throw                            // The "throw" keyword
--                \- new                              // If "new" is present
--                \- exception                        // The exception name
---------------------------------------------------------------------
alias throw = "throw "
alias new = "new "
exception = { !statement_end .}*
exceptions = {{ throw new? exception statement_end } / .}*

-------------------------------
-- Classes
--
-- Pattern to match class definitions. Does not
-- capture class bodies.
--
-- Run: rosie -wholefile classes <java-source-file>
-- Structure: classes                                // Parent pattern that matches all class definitions
--                \- class_definition                // Matches an entire class definition
--                   \- class                        // The word "class"
--                   \- class_name                   // The name of the class being matched
--                     \- parameters                 // Matches the parameters for the function call
--                         \- multip_param           // Matches all parameters
--                             \- single_param       // matches a single parameter
--                   \- extends                      // The word "extends"
-------------------------------
alias class_identifier = "class"
alias extends = "extends"
class_name = { !start_block !start_paren !extends .}*
class = class_identifier class_name parameters? {extends class_name}? start_block

classes = { class / . }*

file = {line_comment / block_comment / string / dependency / function / class / .}*